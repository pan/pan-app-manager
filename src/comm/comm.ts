import { Duplex as DuplexStream } from 'stream'
import { Message, messageType } from './Message'
import { ErrorMessage } from './Message'
import { RequestStreamMessage, forgeRequestStreamMessage, messageAskForEntryStream } from './Message'



var PostMessageStream = require('post-message-stream')
var toBlobURL = require('stream-to-blob-url')

export let init: () => void


//private methods

let createNewStream: (id: string) => DuplexStream

createNewStream = (id: string): DuplexStream =>
{
  console.log('create stream '  + 'iframe-parent-pan-' + id)

  return new PostMessageStream(
    {
      name: 'iframe-child-pan-' + id,
      target: 'iframe-parent-pan-' + id,
      targetWindow: frames.parent,
    }
  )
}

init = (): void =>
{
  if (typeof window === 'undefined')
  {
    return
  }
  let stream: DuplexStream = createNewStream('main')

  stream.on('data', function (chunk: any)
  {
    let message: Message | null = Message.parse(chunk)
    if (message)
    {
      switch (message.getType())
      {
        case messageType.ERROR:
          console.log("[COMM-child] error message received: " + (message as ErrorMessage).getErrorMessage())
        break
        case messageType.REQUEST_STREAM:
          console.log("[COMM-child] request stream message received: this should not happend")
        break
        default:
      }
    }
  })

  let streamEntry: DuplexStream = createNewStream('file-entry')
  let entryHTML = ''

  streamEntry.on('data', function (chunk: any)
  {
    console.log('received ' + chunk)
    entryHTML += chunk
  })

  streamEntry.on('end', function()
  {
    document.body.innerHTML = entryHTML
  })
  stream.write(messageAskForEntryStream)



}
