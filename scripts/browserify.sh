#!/bin/bash

# <!> this script needs to have browserify installed.
# 	to do so :
# 	npm install -g browserify
# 	(you may need to be root)

mkdir -p bin
find dist/src/ -name "*.js" | xargs browserify > bin/pan-app-manager.js
