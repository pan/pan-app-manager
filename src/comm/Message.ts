
export enum messageType
{
  ERROR,
  REQUEST_STREAM,
}

export let forgeErrorMessage: (errorMessage: string) => Message
export let forgeRequestStreamMessage: (fileName: string) => Message
export let messageAskForEntryStream: any


export abstract class Message
{

  //public members
  public getType: () => messageType
  public abstract toSendable: () => any
  public static parse: (mess: any) => Message | null = (mess: any): Message | null =>
  {
    switch (mess.type)
    {
      case messageType.ERROR:
        return ErrorMessage.parse(mess)
      break
      case messageType.REQUEST_STREAM:
        return RequestStreamMessage.parse(mess)
      break
      default:
        return null
    }
  }

  //protected members
  protected type: messageType = messageType.ERROR
  protected constructor(type: messageType)
  {
    this.type = type
    this.getType = (): messageType =>
    {
      return this.type
    }
  }


}

export class ErrorMessage extends Message
{
  message: string
  public getErrorMessage: () => string

  private constructor(message: string)
  {
    super(messageType.ERROR)
    this.message = message
    this.getErrorMessage = (): string =>
    {
      return this.message
    }
  }


  public toSendable: () => any = () : any =>
  {
    return {type: this.type, error: this.message}
  }

  public static forge: (errorMessage: string) => ErrorMessage = (errorMessage: string): ErrorMessage =>
  {
    return new ErrorMessage(errorMessage)
  }

  public static parse: (mess: any) => ErrorMessage = (mess: {type: messageType, error: string}): ErrorMessage =>
  {
    return new ErrorMessage(mess.error)
  }
}

export class RequestStreamMessage extends Message
{
  name: string
  public getFileName: () => string
  public isEntry: () => boolean
  constructor(name: string)
  {
    super(messageType.REQUEST_STREAM)
    this.name = name
    this.getFileName = (): string =>
    {
      return this.name
    }
    this.isEntry = (): boolean =>
    {
      return this.getFileName() === ''
    }
  }

  public toSendable: () => any = () : any =>
  {
    return {type: this.type, name: this.name}
  }

  public static forge: (name: string) => RequestStreamMessage = (name: string): RequestStreamMessage =>
  {
    return new RequestStreamMessage(name)
  }

  public static parse: (mess: any) => RequestStreamMessage = (mess: {type: messageType, name: string}): RequestStreamMessage =>
  {
    return new RequestStreamMessage(mess.name)
  }
}

forgeErrorMessage = ErrorMessage.forge
forgeRequestStreamMessage = RequestStreamMessage.forge
 messageAskForEntryStream = forgeRequestStreamMessage('').toSendable()
