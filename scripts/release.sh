#!/bin/bash

# <!> this script needs to have minify installed.
# 	to do so :
# 	npm install -g minify
# 	(you may need to be root)

mkdir -p bin
uglify -s bin/pan-app-manager.js -o bin/pan-app-manager.min.js
