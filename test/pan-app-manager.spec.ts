import 'mocha'
import { expect } from 'chai'
import { Message,  messageType } from '../src/types'
import { ErrorMessage, forgeErrorMessage } from '../src/types'
import { RequestStreamMessage, forgeRequestStreamMessage } from '../src/types'


describe('Message Error', () =>
{
	it('should forge error message',  () =>
	{
    let message: Message = forgeErrorMessage('test')
    expect(message.getType()).to.be.eq(messageType.ERROR)
    expect(message).to.be.instanceof(ErrorMessage)
    expect((message as ErrorMessage).getErrorMessage()).to.not.be.empty
    expect((message as ErrorMessage).getErrorMessage()).to.be.a('string')
    expect((message as ErrorMessage).getErrorMessage()).to.be.eq('test')

    expect(message.toSendable().type).to.be.eq(messageType.ERROR)
    expect(message.toSendable().error).to.be.eq('test')


	})

  it('should parse error message',  () =>
  {
    let message: Message = forgeErrorMessage('test')
    expect(message.getType()).to.be.eq(messageType.ERROR)
    expect(message).to.be.instanceof(ErrorMessage)
    expect((message as ErrorMessage).getErrorMessage()).to.not.be.empty
    expect((message as ErrorMessage).getErrorMessage()).to.be.a('string')
    expect((message as ErrorMessage).getErrorMessage()).to.be.eq('test')

    let message2: {type: messageType, error: string} = message.toSendable()
    expect(message2.type).to.be.eq(messageType.ERROR)
    expect(message2.error).to.be.eq('test')

    let message3: Message | null = Message.parse(message2)
    expect(message3).to.not.be.null
    expect(message.getType()).to.be.eq(messageType.ERROR)
    expect(message3).to.be.instanceof(ErrorMessage)
    expect((message3 as ErrorMessage).getErrorMessage()).to.not.be.empty
    expect((message3 as ErrorMessage).getErrorMessage()).to.be.a('string')
    expect((message3 as ErrorMessage).getErrorMessage()).to.be.eq('test')
  })

})
describe('Message Request Stream', () =>
{
	it('should forge Request Stream message',  () =>
	{
    let message: Message = forgeRequestStreamMessage('test')
    expect(message.getType()).to.be.eq(messageType.REQUEST_STREAM)
    expect(message).to.be.instanceof(RequestStreamMessage)
    expect((message as RequestStreamMessage).getFileName()).to.not.be.empty
    expect((message as RequestStreamMessage).getFileName()).to.be.a('string')
    expect((message as RequestStreamMessage).getFileName()).to.be.eq('test')

    expect(message.toSendable().type).to.be.eq(messageType.REQUEST_STREAM)
    expect(message.toSendable().name).to.be.eq('test')


	})

  it('should parse Request Stream message',  () =>
  {
    let message: Message = forgeRequestStreamMessage('test')
    expect(message.getType()).to.be.eq(messageType.REQUEST_STREAM)
    expect(message).to.be.instanceof(RequestStreamMessage)
    expect((message as RequestStreamMessage).getFileName()).to.not.be.empty
    expect((message as RequestStreamMessage).getFileName()).to.be.a('string')
    expect((message as RequestStreamMessage).getFileName()).to.be.eq('test')

    let message2: {type: messageType, name: string} = message.toSendable()
    expect(message2.type).to.be.eq(messageType.REQUEST_STREAM)
    expect(message2.name).to.be.eq('test')

    let message3: Message | null = Message.parse(message2)
    expect(message3).to.not.be.null
    expect(message.getType()).to.be.eq(messageType.REQUEST_STREAM)
    expect(message3).to.be.instanceof(RequestStreamMessage)
    expect((message3 as RequestStreamMessage).getFileName()).to.not.be.empty
    expect((message3 as RequestStreamMessage).getFileName()).to.be.a('string')
    expect((message3 as RequestStreamMessage).getFileName()).to.be.eq('test')
  })

})
